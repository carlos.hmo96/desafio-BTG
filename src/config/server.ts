import express from 'express'
import routerLogin  from '../routes/login'
import routerUsuario from  '../routes/usuario'
import dotenv from 'dotenv'

const app = express()

/* configurar o middleware body-parser */
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.use(function(req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'content-type');
  res.setHeader('Access-Control-Allow-Credentials', 'true');
  next();
});

/* importando as Rotas */
app.use(routerLogin)
app.use(routerUsuario)

dotenv.config()

export { app } 