import * as jwt from 'jsonwebtoken'

export const generatedToken = (login: string) => {
    console.log(process.env.SECRET)
    return jwt.sign({ login }, process.env.SECRET || '', { expiresIn: '5m' })
}
