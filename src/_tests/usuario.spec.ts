import { generatedToken } from '../utils/auth';
import { Response } from 'express';
import request from 'supertest';
import { app } from '../config/server'

describe('Gerar Documento', () => {
    it('Testa a função gerarDocumento com campos não informados', (done) => {
        request(app).post('/gerar-documento')
            .send({})
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(400)
            .end((err, res) => {
                if (err) return done(err);
                done();
            });
    })

    it('Testa a função gerarDocumento com todos os campos e token inválido', (done) => {
        request(app).post('/gerar-documento')
            .send({
                nome: 'Teste',
                data_nascimento: '15/03/1999',
                cpf: '12345678999',
                rg: '54334181'
            })
            .set('Accept', 'application/json')
            .set('token', 'vngsd;ojafkm')
            .expect('Content-Type', /json/)
            .expect(401)
            .end((err, res) => {
                if (err) return done(err);
                done();
            });
    })


    it('Testa a função gerarDocumento execução de sucesso', (done) => {

        request(app).post('/gerar-documento')
            .send({
                nome: 'Teste',
                data_nascimento: '15/03/1999',
                cpf: '12345678999',
                rg: '54334181'
            })
            .set('Accept', 'application/json')
            .set('token', generatedToken('teste'))
            .expect('Content-Type', /json/)
            .expect(200)
            .end((err, res) => {
                if (err) return done(err);
                done();
            });
    })
})