import request from 'supertest';
import { app } from '../config/server'

describe('Autenticacao', () => {

    it('Testa a função Login com campos não informados ', (done) => {
        request(app).post('/login')
            .send({})
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(400)
            .end((err, res) => {
                if (err) return done(err);
                done();
            });
    })

    it('Testa a função Login com credenciais válidas', (done) => {
        request(app).post('/login')
            .send({
                login: 'teste',
                senha: '1234'
            })
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200)
            .end((err, res) => {
                if (err) return done(err);
                done();
            });
    })

    it('Testa a função Login com credenciais erradas', (done) => {
        request(app).post('/login')
            .send({
                login: 'teste',
                senha: '12345'
            })
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(401)
            .end((err, res) => {
                if (err) return done(err);
                done();
            });
    })
})