import { Request, Response } from 'express'
import { validationResult } from 'express-validator'
import * as jwt from 'jsonwebtoken'
import * as fs from 'fs';
import * as path from 'path';

export class Usuario {

    gerarDocumento(req: Request, res: Response) {

        const erros = validationResult(req)

        if (!erros.isEmpty()) {
            const key = Object.keys(erros.mapped())[0]
            return res.status(400).json({ message: erros.mapped()[key].msg })
        }

        let { nome, data_nascimento, cpf, rg } = req.body
        let token: any = req.headers.token
        let login = ''
      
        const ip = req.connection.remoteAddress
        
        try {
            jwt.verify(token, process.env.SECRET || '',
                (err, decoded) => {
                    login = decoded.login
                }
            )
        } catch (error) {
            return res.status(401).json({ message: 'Token inválido' })
        }

        let conteudo = `Nome Completo: ${nome} \n` +
            `Data de Nascimento: ${data_nascimento} \n` +
            `CPF: ${cpf} \n` +
            `RG: ${rg} \n \n` +
            `Usuario Autenticado \n` +
            `Login: ${login} \n` +
            `IP: ${ip}`;

        fs.writeFile(path.join(__dirname, '../../arquivo.txt'), conteudo, (err) => {
            if (err)
                return res.status(400).json({ message: 'Erro ao gravar o arquivo' })

            return res.status(200).json({ message: 'arquivo gravado com sucesso', url: path.join(__dirname, '../../arquivo.txt') })
        })

    }
}
