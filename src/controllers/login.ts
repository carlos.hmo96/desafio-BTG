import { generatedToken } from '../utils/auth';
import { Request, Response } from 'express'
import { validationResult } from 'express-validator'

export class Login {

    login(req: Request, res: Response) {

        const erros = validationResult(req)

        if (!erros.isEmpty()) {
            const key = Object.keys(erros.mapped())[0]
            return res.status(400).json({ message: erros.mapped()[key].msg })
        }

        let { login, senha } = req.body
        
        if (login === 'teste' && senha === '1234'){
            let token = generatedToken(login);
            return res.status(200).json({ token: token })
        }else{
            return res.status(401).json({ message: 'login e/ou senha inválidos' })
        }
    }
}