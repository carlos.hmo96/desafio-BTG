import { Usuario } from './../controllers/usuario';
import { Request, Response, Router } from "express";
import { gerarDocumentoValidator } from './validators/usuario'

const routerUsuario = Router();
const usuario = new Usuario();

routerUsuario.post('/gerar-documento', gerarDocumentoValidator, (req: Request, res: Response) => {
    usuario.gerarDocumento(req, res)
})

export default routerUsuario;
