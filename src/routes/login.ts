import { Request, Response, Router } from "express";
import { loginValidator } from './validators/login'
import { Login } from '../controllers/login'

const routerLogin = Router();
const login =  new Login();

routerLogin.post('/login', loginValidator, (req: Request, res: Response) => {
    login.login(req , res)
})

export default routerLogin;

