import { check, header } from 'express-validator'

export const gerarDocumentoValidator = [
    header('token', 'token não infornmado').not().isEmpty(),
    check('nome', 'O nome login é de preenchimento obrigatório').not().isEmpty(),
    check('data_nascimento', 'O data de nascimento senha é de preenchimento obrigatório').not().isEmpty(),
    check('cpf', 'O campo cpf é de preenchimento obrigatório').not().isEmpty(),
    check('rg', 'O campo rg é de preenchimento obrigatório').not().isEmpty(),
]