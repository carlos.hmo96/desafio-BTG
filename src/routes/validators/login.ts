import { check, body , header } from 'express-validator'

export const loginValidator = [
    check('login', 'O campo login é de preenchimento obrigatório').not().isEmpty(),
    check('senha', 'O campo senha é de preenchimento obrigatório').not().isEmpty(),
]